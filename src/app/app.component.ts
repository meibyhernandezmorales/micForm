import { Component, OnInit, ViewChild } from '@angular/core';
import { MenubarModule } from 'primeng/menubar';
import { MenuItem } from 'primeng/api';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

interface Year {
  date: string;
}

interface Public {
  targetPeople: string;
}

interface Genre {
  genre: string;
}

interface Type {
  type: string;
}

interface Circulation {
  circulation: string;
}

interface City {
  cityName: string;
}

interface Checkbox {
  option: string;
  key: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  items: MenuItem[];
  userForm: FormGroup;
  years: Year[];
  people: Public[];
  genres: Genre[];
  types: Type[];
  circulations: Circulation[];
  namesCity: City[];
  secondchecks: Checkbox[];
  formData: any;
  showDivChecks: false;
  wordCount = { count: 0 };
  wordCount1 = { count: 0 };
  text: string;

  constructor(private fb: FormBuilder) {
    this.text = '';
  }

  ngOnInit() {
    this.items = [
      {
        label: 'File',
        icon: 'pi pi-fw pi-file',
      },
      {
        label: 'Edit',
        icon: 'pi pi-fw pi-pencil',
      },
      {
        label: 'Users',
        icon: 'pi pi-fw pi-user',
      },
      {
        label: 'Events',
        icon: 'pi pi-fw pi-calendar',
      },
      {
        label: 'Quit',
        icon: 'pi pi-fw pi-power-off',
      },
    ];

    this.secondchecks = [
      {
        option: 'Primera Infancia (0-5 años)',
        key: 'Primera',
      },
      {
        option: 'Infancia (6 - 11 años)',
        key: 'Segunda',
      },
      {
        option: 'Adolescencia (12 - 18 años)',
        key: 'Tercera',
      },
      {
        option: 'Juventud (14 - 26 años)',
        key: 'Cuarta',
      },
      {
        option: 'Adultez (27- 59 años)',
        key: 'Quinta',
      },
    ];

    this.years = [
      { date: '2023' },
      { date: '2022' },
      { date: '2021' },
      { date: '2020' },
      { date: '2019' },
    ];

    this.people = [
      { targetPeople: 'Infantil' },
      { targetPeople: 'Juvenil' },
      { targetPeople: 'Adulto' },
      { targetPeople: 'Familiar' },
      { targetPeople: 'Tercera edad' },
      { targetPeople: 'Empresas' },
      { targetPeople: 'Estado' },
    ];

    this.genres = [
      { genre: 'Educativos' },
      { genre: 'Terror' },
      { genre: 'Infantil' },
    ];

    this.types = [{ type: 'Tipo 1' }, { type: 'Tipo 2' }];

    this.circulations = [
      { circulation: 'Nacional' },
      { circulation: 'Departamental' },
      { circulation: 'Municipal' },
    ];

    this.userForm = this.fb.group({
      productName: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(200),
        this.countletters,
      ]),
      presentationDate: new FormControl(<Year | null>null, [
        Validators.required,
      ]),
      city: ['', Validators.required],
      verification: ['', Validators.required],
      targetPublic: new FormControl(<Public | null>null, [Validators.required]),
      literaryGender: new FormControl(<Genre | null>null, [
        Validators.required,
      ]),
      researchProject: new FormControl('', [
        Validators.required,
        this.countletters,
      ]),
      type: new FormControl(<Type | null>null, [Validators.required]),
      circulation: new FormControl(<Circulation | null>null, [
        Validators.required,
      ]),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(400),
        this.wordsValidation,
      ]),
      conceptualization: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(400),
        this.wordsValidation,
      ]),
      differentialapproach: [false],
      differentialapproachOptions: [''],
    });

    this.countWords('description', this.wordCount);
    this.countWords('conceptualization', this.wordCount1);
  }

  countWords(textarea, wordCount) {
    this.userForm.get(textarea).valueChanges.subscribe((val) => {
      wordCount.count = val.split(/\s+/).filter((word) => word !== '').length;
    });
  }

  sendData() {
    this.formData = JSON.stringify(this.userForm.value);
  }

  wordsValidation(control: FormControl): { [key: string]: boolean } | null {
    const palabras = control.value.trim().split(/\s+/).length;
    if (palabras < 10 || palabras > 400) {
      return { numeroPalabrasInvalido: true };
    }
    return null;
  }

  countletters(control: FormControl): { [key: string]: boolean } | null {
    const text = control.value;
    const letters = text.replace(/ /g, '').length;
    if (letters < 10 || letters > 200) {
      return { numeroLetrasInvalido: true };
    }
    return null;
  }
}
